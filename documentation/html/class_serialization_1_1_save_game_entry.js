var class_serialization_1_1_save_game_entry =
[
    [ "SaveGameEntry", "class_serialization_1_1_save_game_entry.html#a20e30c624cc3c5a43565f55aa5fc0670", null ],
    [ "GetBool", "class_serialization_1_1_save_game_entry.html#a272a8e9f6ddfaf989a872d0bff357088", null ],
    [ "GetFloat", "class_serialization_1_1_save_game_entry.html#af46900ca3d6bce38166f183d24a025c5", null ],
    [ "GetInt", "class_serialization_1_1_save_game_entry.html#aef137d33f9f6b14bc3955d348b7aea60", null ],
    [ "GetString", "class_serialization_1_1_save_game_entry.html#a175bea895a50370a1a401b8851f33c9e", null ],
    [ "GetValue", "class_serialization_1_1_save_game_entry.html#abb185ae8cd79ac67ec659cedacde836d", null ],
    [ "IsEmpty", "class_serialization_1_1_save_game_entry.html#a513eaa1b9482e27725f73ccc109e4b44", null ],
    [ "Set", "class_serialization_1_1_save_game_entry.html#a70d75fa7ec7d11a59f4c49d716818e38", null ],
    [ "mId", "class_serialization_1_1_save_game_entry.html#a2b310c473a8201b3179fe7337528f400", null ],
    [ "mKeys", "class_serialization_1_1_save_game_entry.html#aa6d1d5603eb1078f07e09ba102aeceb7", null ],
    [ "mValues", "class_serialization_1_1_save_game_entry.html#aa517579d5dcc3194764e00596961064c", null ],
    [ "mVersion", "class_serialization_1_1_save_game_entry.html#adceacf9f66434b9c893f6a246038f340", null ]
];