var class_u_e_d_s_1_1_settings_proxy =
[
    [ "SettingsProxy", "class_u_e_d_s_1_1_settings_proxy.html#aef4551d8d2f459a5ced24f455d359360", null ],
    [ "AddInstance", "class_u_e_d_s_1_1_settings_proxy.html#ac3e29d58ccd3718100d0ae5a725e95ff", null ],
    [ "DelInstance", "class_u_e_d_s_1_1_settings_proxy.html#ab2c5384037c72bfd71e1ab1afedcc875", null ],
    [ "DupInstance", "class_u_e_d_s_1_1_settings_proxy.html#a8460dc4829bbd6d2c2474ffaef79957f", null ],
    [ "GetContainerForType", "class_u_e_d_s_1_1_settings_proxy.html#a1c5a9dfa878808def94e05e93f2c91dd", null ],
    [ "GetInstanceNames", "class_u_e_d_s_1_1_settings_proxy.html#a1616c4ae05939498fb5a0272509f9b87", null ],
    [ "Save", "class_u_e_d_s_1_1_settings_proxy.html#a1875877f9b8adbb04b47db9c2fd4a21e", null ],
    [ "settingContainers", "class_u_e_d_s_1_1_settings_proxy.html#adf1c767dd3d4f6615209e750188c8151", null ]
];